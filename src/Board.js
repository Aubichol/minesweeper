import React from 'react';

class Square extends React.Component {

        render(){
                let color = "square" ;
                let show = this.props.value ;
                if( this.props.value === 0 ) {
                        color = "black"
                        show = "" ;
                }
                return(
                        <button className={color} onClick={() => this.props.onClick()} onContextMenu={(e,i)=>this.props.onContextMenu(e,i)}>
                                {show}
                        </button>
                );
        }
}

export default class Board extends React.Component {
        renderSquare(i) {
                return <Square value={this.props.squares[i]} key={i} onClick={() => this.props.onClick(i)} onContextMenu={(e) => this.props.onContextMenu(e, i)}/>;
        }

        renderRow(low, high) {
                let contents = [];

                for(let i = low; i < high; i++) {
                        contents = [
                                ...contents,
                                this.renderSquare(i)
                        ]
                }
                return (
                        <div key={low} className="board-row">
                                {contents}
                        </div>
                );
        }

        renderBoard(start, end, jump) {
                let contents = [];

                for(let i = start; i < end; i+=jump) {
                        contents = [
                                ...contents,
                                this.renderRow(i, i+jump)
                        ];
                }
                return (
                        <div className="complete-board">
                                {contents}
                        </div>
                );
        }

        render() {
                return this.renderBoard(0, 100, 10);
        }
}
