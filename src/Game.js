import React from 'react';
import Board from "./Board.js";

const BOARDROW = 10 ;
const BOARDCOL = 10 ;

var visited = new Array(BOARDROW*BOARDCOL).fill(null) ;
var dfs_id = 1 ;
var mines = new Array(BOARDROW*BOARDCOL) ;


export default class Game extends React.Component {

        // contructor initializes Game component and related states
        constructor(props) {
                super(props);

                let w = new Array(BOARDROW*BOARDCOL).fill(null);
                let x = new Array(BOARDROW*BOARDCOL).fill(null);
                let y = new Array(BOARDROW*BOARDCOL).fill(null);
                let z = new Array(BOARDROW*BOARDCOL).fill(null);
                let cnt = 0 ;

                [ x, cnt ] = this.getInitStates();

                this.ROWS = [ -1, -1, -1, 0 , 0 , 1 , 1 , 1 ] ;
                this.COLS = [ -1 , 0 , 1 , -1 , 1 , -1 , 0 , 1 ] ;
                this.calculateMines(x);

                this.state = {
                        squares: x,
                        revealed: y,
                        clicked: z,
                        gameOver: false,
                        numbers: w,
                        mineCount: cnt,
                        won: false
                }
        }

        // getInitStates sets mines randomly to a board
        getInitStates() {
                let cnt = 0 ;
                let x = new Array(BOARDROW*BOARDCOL);
                for( let i = 0 ; i < (BOARDROW*BOARDCOL) ; i ++ ) {
                       let tmp = Math.floor(Math.random()*20);

                       if( tmp < 5 ) {
                               x[ i ] = 1 ;
                               cnt ++ ;
                       } else {
                               x[ i ] = 0 ;
                       }
                }
                return [x, cnt] ;
        }

        // calculateMines counts for surrounding mines for a cell
        calculateMines(x) {
                for( let i = 0 ; i < BOARDROW ; i ++ ) {
                        for( let j = 0 ; j < BOARDCOL ; j ++ ) {
                                let tot = 0 ;
                                let cur_pos = ( i * BOARDCOL ) + j ;

                                for( let k = 0 ; k < 8 ; k ++ ) {
                                        let n_r = i + this.ROWS[ k ] ;
                                        let n_c = j + this.COLS[ k ] ;
                                        let nxt_pos = ( n_r * BOARDCOL ) + n_c ;

                                        if( this.withinLimit( n_r , n_c ) && x[ nxt_pos ] === 1 ) {
                                                tot ++ ;
                                        }
                                }
                                mines[ cur_pos ] = tot ;
                        }
                }
        }

        // isClear tests whether all the cells around a cell is free of mines
        isClear( r , c ) {
                for( let i = 0 ; i < 8 ; i ++ ) {
                        let n_r = i + this.ROWS[ i ] ;
                        let n_c = i + this.COLS[ i ] ;

                        if( this.withinLimit( n_r , n_c ) === false ) {
                                continue ;
                        }

                        if( this.isMine( n_r , n_c ) ) {
                                return false ;
                        }
                }
                return true ;
        }

        // get_row returns imaginary 2D array's row
        get_row( i ) {
                return Math.floor(i / BOARDCOL) ;
        }

        // get_col returns imaginary 2D array's column
        get_col( i ) {
                return i % BOARDCOL ;
        }

        // withinLimit checks whether a cell is within the imaginary limit of the board
        withinLimit( r , c ) {
                if( r >= 0 && r < BOARDROW && c >= 0 && c < BOARDCOL ) return true ;
                return false ;
        }

        // isVisited checks whether a cell was visited by any DFS before
        isVisited( r , c ) {
                const cur_pos = ( r * BOARDCOL ) + c ;
                if( visited[ cur_pos ] > 0 ) {
                        return true ;
                }
                return false ;
        }

        // isMine checks whether a cell contains a mine
        isMine( r , c ) {
                const cur_pos = ( r * BOARDCOL ) + c ;
                if( this.state.squares[ cur_pos ] === 1 ) {
                        return true ;
                }
                return false ;
        }

        // isRevealed checks whether a cell is revealed yet
        isRevealed( r , c ) {
                const cur_pos = ( r * BOARDCOL ) + c ;
                if( this.state.revealed[ cur_pos ] ) {
                        return true ;
                }
                return false ;
        }

        // revealOneBox reveals one box during a DFS
        revealOneBox( r , c ) {
                const cur_pos = ( r * BOARDCOL ) + c ;
                visited[ cur_pos ] = dfs_id ;
        }

        // recur starts looking for an adjacent empty cell from the left-clicked cell
        recur( row, col ) {
                //const cur_pos = ( row * 10 ) + col ;
                this.revealOneBox( row , col ) ;
                console.log("Entering :"+row+" "+col)
                for( let i = 0 ; i < 8 ; i ++ ) {
                        let n_r = row + this.ROWS[ i ] ;
                        let n_c = col + this.COLS[ i ] ;

                        if( this.withinLimit( n_r , n_c ) === false ) {
                                continue ;
                        }

                        if( this.isVisited( n_r , n_c ) === true ) {
                                continue ;
                        }
                        if( this.isMine( n_r, n_c ) ) {
                                continue ;
                        }

                        if( this.isRevealed( n_r , n_c ) ) {
                                continue ;
                        }

                        if( this.isClear( n_r , n_c ) === false ) {
                                this.revealOneBox(n_r,n_c);
                                continue ;
                        }


                        console.log(n_r + " " + n_c) ;
                        this.recur(n_r,n_c);
                }
        }

        // reveal start revealing squares from position i
        reveal( i ) {
                let cur_row = this.get_row( i ) ;
                let cur_col = this.get_col( i ) ;

                this.recur(cur_row, cur_col) ;
                dfs_id ++ ;
        }


        // Checks whether the game is over
        gameOver() {
                return this.state.gameOver ;
        }

        // makeGameOver ends the game when the user clicks on a mine cell
        makeGameOver() {
                console.log("game ends");
                const cur_numbers = this.state.numbers.slice() ;

                for( let i = 0 ; i < (BOARDROW * BOARDCOL ) ; i ++ ) {
                        if( this.state.squares[ i ] === 1 ) {
                                cur_numbers[ i ] = '*' ;
                        }
                }
                this.setState({
                        gameOver: true,
                        numbers: cur_numbers,
                });
        }

        // makeClick sets the mine in a right clicked cell
        makeClick(r, c) {
                const cur_clicked = this.state.clicked.slice() ;
                const cur_numbers = this.state.numbers.slice() ;
                const cur_mine_count = this.state.mineCount ;
//                const cur_clicked = this.state.clicked.slice() ;
                const cur_pos = ( r * BOARDCOL ) + c ;

                cur_clicked[ cur_pos ] = 1 ;
                cur_numbers[ cur_pos ] = 'M' ;

                return [ cur_clicked, cur_numbers , cur_mine_count - 1 ] ;
        }

        // changeBoard changes the board after a left-clicked DFS
        changeBoard() {
                const rev = this.state.revealed.slice();
                const num = this.state.numbers.slice() ;

                for( let i = 0 ; i < (BOARDROW*BOARDCOL) ; i ++ ) {
                        if( visited[ i ] === ( dfs_id - 1 ) ) {
                                rev[ i ] = 1 ;
                                num[ i ] = mines[ i ] ;
                        }
                }

                this.setState({
                        revealed: rev,
                        numbers: num,
                });
        }

        // handles left click
        handleClick(i) {
                console.log("Clicked : " + i );
                if ( this.gameOver() ) {
                        console.log("Game is already over!");
                        return ;
                }

                const cur_row = this.get_row( i ) ;
                const cur_col = this.get_col( i ) ;

                if ( this.isRevealed( cur_row , cur_col ) ) {
                        return ;
                }
                if ( this.isMine( cur_row , cur_col ) ) {
                        this.makeGameOver();
                        return ;
                }
                this.reveal( i );
                this.changeBoard();
        }

        // isFinished checks whether a user has successfully swept all the mines
        checkFinished(clicked) {
                console.log(this.state.squares.filter(t => t));
                console.log(this.state.clicked.filter(t => t));
                for( let i = 0 ; i < BOARDROW ; i ++ ) {
                        for( let j = 0 ; j < BOARDCOL ; j ++ ) {
                                let cur_pos = ( i * BOARDCOL ) + j ;
                                if( this.state.squares[ cur_pos ] === 1 ) {

                                        if( clicked[ cur_pos ] === null ) {

                                                return false ;
                                        }
                                }
                        }
                }
                return true ;
        }

        // finish changes the game state "won" when the user successfully finishes the game
        finish() {
                this.setState({
                        won: true,
                });
        }

        // isClicked checks whether a cell has been already right clicked
        isClicked( row , col ) {
                let cur_pos = ( row * BOARDCOL ) + col ;
                return this.state.clicked[ cur_pos ] ;
        }

        // handleRightClick handles the right click
        handleRightClick(e, i) {
                e.preventDefault();
                if ( this.gameOver() ) {
                        return ;
                }

                let cur_row = this.get_row( i ) ;
                let cur_col = this.get_col( i ) ;

                if ( this.isRevealed( cur_row, cur_col ) ) {
                        return ;
                }
                if ( this.isClicked( cur_row , cur_col ) ) { // no uncheck option for now, will fix later
                        return ;
                }

                let cur_clicked ;
                let cur_numbers ;
                let cur_won ;
                let cur_mine_count ;

                [ cur_clicked, cur_numbers, cur_mine_count ] = this.makeClick( cur_row , cur_col );
                cur_won = this.checkFinished( cur_clicked );

                this.setState({
                        clicked: cur_clicked,
                        numbers: cur_numbers,
                        won: cur_won,
                        mineCount: cur_mine_count,
                });
        }

        // isFinished checks whether a game has been finished
        isFinished() {
                return this.state.won ;
        }

        // render renders game component
        render() {
                let won = "Game is going on!" ;
                if( this.isFinished() ) {
                        won = "You have won! :D" ;
                } else if( this.gameOver() ) {
                        won = "Sorry! You have lost the game! Try again?"
                }

                return (
                        <div className="game">
                                <Board squares={this.state.numbers} onClick={(i)=>this.handleClick(i)} onContextMenu={(e, i)=>this.handleRightClick(e, i)}>
                                </Board>
                                <div className="game-info">
                                        <div>{won}</div>
                                        <div><p>Mines Left: {this.state.mineCount} </p> </div>
                                </div>
                        </div>
                );
        }
}
